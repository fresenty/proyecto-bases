<?php

require_once "conexionBD.php";


class ExamenM extends ConexionBD{

    static public function BorrarDatosM($datosC){
        $pdo = ConexionBD::cBD()->prepare("DELETE FROM `presenta` WHERE cedula = :cedula");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);
        if($pdo -> execute()){
            return "Bien";
        }else{
            return "Bien";
        }
        $pdo->close();
    }

    static public function GuardarRespuestasM($datosG, $tablaGBD){
        $pdo = ConexionBD::cBD()->prepare("INSERT INTO $tablaGBD (cedula, id_pre, res_e) VALUES (:cedula, :id_pre, :res_e)");
        $pdo -> bindParam(":cedula", $datosG["cedula"], PDO::PARAM_INT);
        $pdo -> bindParam(":id_pre", $datosG["id_pre"], PDO::PARAM_STR);
        $pdo -> bindParam(":res_e", $datosG["res_e"], PDO::PARAM_STR);
        if($pdo -> execute()){
            return "Bien";
        }else{
            return "Error";
        }
        $pdo->close();
    }

    static public function PreguntasM($datosC, $tablaBD){

        $pdo = ConexionBD::cBD()->prepare("SELECT id_pre, des_pre, res_a, res_b, res_c, res_d FROM $tablaBD WHERE id_pre = :id_pre");

        $pdo -> bindParam(":id_pre", $datosC, PDO::PARAM_INT);
        $pdo -> execute();
        return $pdo -> fetch();
        $pdo->close();
    }
}
?>