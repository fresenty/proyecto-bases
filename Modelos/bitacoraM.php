<?php

require_once "conexionBD.php";

class bitacoraM extends ConexionBD{

    //Mostrar Bitacora
    static public function MostrarbitacoraM($tablaBD)
    {

        $pdo = ConexionBD::cBD()->prepare("SELECT P.id_bitacora, P.cedula, P.fecha, P.ejecutor, P.actividad_realizada, P.informacion_actual, P.informacion_anterior FROM $tablaBD AS P ; ");

        $pdo->execute();

        return $pdo->fetchAll(); // fetchALL para pedir todas las filas

        $pdo->close();
    }

}
