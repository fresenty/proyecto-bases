<?php
require_once "conexionBD.php";

class EstudiantesM extends ConexionBD
{

    //Registrar Estudiantes


    static public function RegistrarEstudiantesM($datosC, $tablaBD)
    {

        $pdo = ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (cedula, usuario, nombres, apellidos, edad, sexo, tipo_persona) VALUES
        (:cedula, :usuario, :nombres, :apellidos, :edad, :sexo, 2)");
        $pdo->bindParam(":cedula", $datosC["cedula"], PDO::PARAM_STR);
        $pdo->bindParam(":usuario", $datosC["usuario"], PDO::PARAM_STR);
        $pdo->bindParam(":nombres", $datosC["nombres"], PDO::PARAM_STR);
        $pdo->bindParam(":apellidos", $datosC["apellidos"], PDO::PARAM_STR);
        $pdo->bindParam(":edad", $datosC["edad"], PDO::PARAM_STR);
        $pdo->bindParam(":sexo", $datosC["sexo"], PDO::PARAM_STR);


        if ($pdo->execute()) {

            return "Bien";

        } else {

            return "Error";
        }
        $pdo->close();
    }

    static public function RegistrarCredencialesM($datosC, $tablaBD)
    {


        $pdo = ConexionBD::cBD()->prepare("INSERT INTO $tablaBD(id_cred, correo, clave, cedula ) VALUES (default, :correo, :clave, :cedula)");

        $pdo->bindParam(":correo", $datosC["correo"], PDO::PARAM_STR);
        $pdo->bindParam(":clave", $datosC["clave"], PDO::PARAM_STR);
        $pdo->bindParam(":cedula", $datosC["cedula"], PDO::PARAM_STR);


        if ($pdo->execute()) {

            return "Bien";

        } else {

            return "Error";
        }

        $pdo2->close();

    }


    //Mostrar Estudiantes
    static public function MostrarEstudiantesM($tablaBD)
    {

        $pdo = ConexionBD::cBD()->prepare("SELECT P.cedula, P.usuario, P.nombres, P.apellidos, P.edad, P.sexo, cre.correo, 
         cre.clave FROM $tablaBD AS P INNER JOIN credenciales AS cre ON P.cedula= cre.cedula where tipo_persona ='2'; ");

        $pdo->execute();

        return $pdo->fetchAll(); // fetchALL para pedir todas las filas

        $pdo->close();
    }

    //Editar Estudiantes

    static public function EditarEstudiantesM($datosC, $tablaBD){

        $pdo = ConexionBD::cBD()->prepare("SELECT P.cedula, P.usuario, P.nombres, P.apellidos, P.edad, P.sexo, 
        cre.correo, cre.clave FROM $tablaBD  AS P INNER JOIN credenciales AS cre ON P.cedula= cre.cedula WHERE P.cedula = :cedula and tipo_persona='2' ");

        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);

        $pdo -> execute();

        return $pdo-> fetch();

        $pdo -> close();

    }

    //Actualizar Estudiante

    static public function ActualizarEstudiantesM($datosC, $tablaBD)
    {


        $pdo = ConexionBD::cBD()->prepare("UPDATE $tablaBD AS P INNER JOIN credenciales AS cre ON P.cedula= cre.cedula SET
         P.cedula = :cedula, usuario = :usuario, nombres = :nombres, apellidos = :apellidos, edad = :edad, sexo = :sexo, clave = :clave, correo = :correo 
         WHERE P.cedula = :cedula");

        $pdo->bindParam(":cedula", $datosC["cedula"], PDO::PARAM_STR);
        $pdo->bindParam(":usuario", $datosC["usuario"], PDO::PARAM_STR);
        $pdo->bindParam(":clave", $datosC["clave"], PDO::PARAM_STR);
        $pdo->bindParam(":correo", $datosC["correo"], PDO::PARAM_STR);
        $pdo->bindParam(":nombres", $datosC["nombres"], PDO::PARAM_STR);
        $pdo->bindParam(":apellidos", $datosC["apellidos"], PDO::PARAM_STR);
        $pdo->bindParam(":edad", $datosC["edad"], PDO::PARAM_STR);
        $pdo->bindParam(":sexo", $datosC["sexo"], PDO::PARAM_STR);

        if ($pdo->execute()) {

            return "Bien";

        } else {

            return "Error";

        }

        $pdo->close();


    }
    static public function BorrarEstudiantesM($datosC, $tablaBD){

        $pdo = ConexionBD::cBD()->prepare("DELETE FROM $tablaBD WHERE cedula = :cedula");

        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);

        if($pdo -> execute()){

            return "Bien";

        }else{

            return "Error";

        }

        $pdo -> close();

    }
    //Mostrar contenido vista
    static public function vistamascuM($tablaBD, $a)
    {

        $pdo = ConexionBD::cBD()->prepare("SELECT P.COUNT(*) FROM $tablaBD AS P ; ");

        $pdo->execute();

        return $pdo->fetchAll(); // fetch para pedir solo un dato
        $pdo->close();
    }

}