<?php
require_once "conexionBD.php";

class graficM extends ConexionBD
{


    static public function MostrargraficM($tablaBD)
    {
        $pdo = ConexionBD::cBD()->prepare("SELECT COUNT(*) FROM $tablaBD WHERE edad>'18'and edad<'40'");
        $pdo->execute();
        $resultado = $pdo->fetchall();
        $pdo2 = ConexionBD::cBD()->prepare("SELECT COUNT(*) FROM $tablaBD WHERE edad>'40'");
        $pdo2->execute();
        $resultado2 = $pdo2->fetchall();
        $mi_array=array("Mayores"=>$resultado[0][0], "Adultos de tercera edad"=>$resultado2[0][0]);

        //var_dump($mi_array);

        return $mi_array; //->fetchAll(); // fetchALL para pedir todas las filas

        $pdo->close();
        $pdo2->close();
    }
    static public function Mostrargrafic2M($tablaBD)
    {
        $pdo = ConexionBD::cBD()->prepare("SELECT COUNT(*) FROM $tablaBD WHERE sexo='femenino'");
        $pdo->execute();
        $resultado = $pdo->fetchall();
        $pdo2 = ConexionBD::cBD()->prepare("SELECT COUNT(*) FROM $tablaBD WHERE sexo='masculino'");
        $pdo2->execute();
        $resultado2 = $pdo2->fetchall();
        $mi_array=array("femenino"=>$resultado[0][0], "masculino"=>$resultado2[0][0]);

        //var_dump($mi_array);

        return $mi_array; //->fetchAll(); // fetchALL para pedir todas las filas

        $pdo->close();
        $pdo2->close();
    }
    static public function Mostrargrafic3M($tablaBD)
    {
        $pdo = ConexionBD::cBD()->prepare("SELECT COUNT(*) FROM $tablaBD WHERE tipo_persona='1'");
        $pdo->execute();
        $resultado = $pdo->fetchall();
        $pdo2 = ConexionBD::cBD()->prepare("SELECT COUNT(*) FROM $tablaBD WHERE tipo_persona='2'");
        $pdo2->execute();
        $resultado2 = $pdo2->fetchall();
        $mi_array=array("Administradores"=>$resultado[0][0], "Estudiantes"=>$resultado2[0][0]);

        //var_dump($mi_array);

        return $mi_array; //->fetchAll(); // fetchALL para pedir todas las filas

        $pdo->close();
        $pdo2->close();
    }
}