<?php
require_once "conexionBD.php";

class ResultadosM extends ConexionBD{

    static public function RespuetasVM($datosC){
        $pdo=ConexionBD::cBD()->prepare("SELECT count(*) as res FROM pregunta p JOIN presenta pr ON p.id_pre = pr.id_pre WHERE res_v = res_e AND cedula = :cedula");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);
        $pdo -> execute();
        return $pdo -> fetch();
        $pdo->close();
    }
    static public function LlenarScoreM($datosC,$res){
        $pdo = ConexionBD::cBD()->prepare("
			INSERT INTO `nota`(`id_nota`, `cedula`, `nota`, `fecha`) VALUES (null, :cedula, :nota, NOW())");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_STR);
        $pdo -> bindParam(":nota", $res, PDO::PARAM_STR);
        if($pdo -> execute()){
            return "Bien";
        }else{
            return "Error";
        }
        $pdo->close();
    }

    static public function MostrarDatosM($datosC,$tablaBD){
        $pdo = ConexionBD::cBD()->prepare("SELECT cedula, nombres, apellidos FROM $tablaBD WHERE cedula = :cedula");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);
        $pdo -> execute();
        return $pdo -> fetch();
        $pdo->close();
    }

    public static function MostrarRespuestasM($datosC){
        $pdo=ConexionBD::cBD()->prepare("SELECT des_pre, res_v, res_e FROM pregunta p JOIN presenta pr ON p.id_pre = pr.id_pre WHERE pr.cedula = :cedula");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);
        $pdo -> execute();
        return $pdo -> fetchAll();
        $pdo -> close();
    }
    public static function MostrarNotaM($datosC){
        $pdo=ConexionBD::cBD()->prepare("SELECT nota FROM nota WHERE cedula = :cedula AND id_nota = (SELECT MAX(id_nota) FROM nota)");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);
        $pdo -> execute();
        return $pdo -> fetch();
        $pdo->close();

    }
    public static function MostrargraficaM($datosC){
        $pdo=ConexionBD::cBD()->prepare("SELECT nota FROM nota WHERE cedula = :cedula AND id_nota = (SELECT MAX(id_nota) FROM nota)");
        $pdo -> bindParam(":cedula", $datosC, PDO::PARAM_INT);
        $pdo -> execute();
        return $pdo -> fetch();
        $pdo->close();

    }
}

?>