<?php

session_start();

$mostrar = new  graficC();
$datos_grafic3 = $mostrar -> Mostrargrafic3C();
$datos_grafic2 = $mostrar -> Mostrargrafic2C();
$datos_grafic = $mostrar -> MostrargraficC();

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Biologia</title>

    <link rel="stylesheet" type="text/css" href="Vistas/css/estilos.css">

    <script src="Vistas/code/jquery-3.2.1.min.js"></script>
    <script src="Vistas/code/highcharts.js"></script>
    <script src="Vistas/code/exporting.js"></script>

</head>

<body>


<?php



if (isset($_SESSION["ADMIN"])) {

    if ($_SESSION["ADMIN"]) {

        include "modulos/menu.php";
    }
}

?>

<section>


    <?php

    $rutas = new RutasControlador();
    $rutas -> Rutas();


    ?>

</section>


<script type="text/javascript">
    Highcharts.chart('pastel',{
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Estadistica de Edad'
        },
        series: [
            {
                type:'pie',
                name: 'Cantidad',
                data:[
                    <?php
                    $array = array("adultos", "Adultos mayores");
                    $i=0;
                    foreach ($datos_grafic as $value) {
                        echo "['".$array[$i]."',".$value."],";
                        $i++;
                    }
                    ?>
                ]
            }
        ]
    });

</script>
<script type="text/javascript">
    Highcharts.chart('pastel2',{
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Estadistica de Sexo'
        },
        series: [
            {
                type:'pie',
                name: 'Cantidad',
                data:[
                    <?php
                    $array = array("femenino", "masculino");
                    $i=0;
                    foreach ($datos_grafic2 as $value) {
                        echo "['".$array[$i]."',".$value."],";
                        $i++;
                    }
                    ?>
                ]
            }
        ]
    });

</script>
</script>
<script type="text/javascript">
    Highcharts.chart('pastel3',{
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Estadistica de Tipos de Persona'
        },
        series: [
            {
                type:'pie',
                name: 'Cantidad',
                data:[
                    <?php
                    $array = array("Administradores", "Estudiantes");
                    $i=0;
                    foreach ($datos_grafic3 as $value) {
                        echo "['".$array[$i]."',".$value."],";
                        $i++;
                    }
                    ?>
                ]
            }
        ]
    });

</script>

</body>

</html>