<?php


session_start();

if(!$_SESSION["ADMIN"]  ){

    header("location:index.php?ruta=ingreso");

    exit();
}


?>
<div class="contenedor">


<div class="caja1">
    <img class="avatarHome" src="Vistas/img/anime.png"  alt="Home">



</div>

<div class="caja2">



    <p>También denominada glicólisis, es una de las principales rutas metabólicas
        productora de energía para todos los seres vivos tanto procariotas como eucariotas
        y ocurre en el citosol. Es una secuencia de diez reacciones catalizadas por enzimas
        distintas, a las primeras 5 se le llama etapa de preparación que consume 2 ATP y a
        las 5 reacciones restantes se le denomina etapa de beneficio produciendo 4 ATP y 2
        NADH, convirtiendo así una molécula de  glucosa en dos piruvatos y obteniendo una
        producción neta de 2 ATP y 2 NADH.
    </p>
    <h1>
        REACCIÓN GLOBAL:
    </h1>
    <p class="p2">
        Glucosa +  2NAD<sup>+</sup> + 2ADP + 2P<sub>i</sub>  2piruvatos + 2NADH + 2H<sup>+</sup>  + 2ATP + 2H<sub>2</sub>O
    </p>

    <h2>
        PASOS DE GLICOLISIS
    </h2>

</div>

<div class="MapaConceptual">

    <img src="Vistas/img/Biologia.png" usemap="#image_map">

    <map name="image_map">
        <area alt="Glucosa" title="Glucosa" href="#openmodal" coords="375,19,550,61" shape="rect">


        <! -- **************************************************Sección Glucosa***************************************************** -->
    <section id="openmodal" class="modalDialog">

        <section class="modal">
            <a href="#close" class="close"> X </a>
            <div class="son">
            <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play1').play(); return false;"  />
            <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play1').pause(); return false;" />
            </div>
            <?php
            $a = 1;
            $mostrar = new  HomeC();
            $mostrar -> MostrarContenidoC($a);
            ?>
        </section>
    </section>
        <! -- **************************************************Sección Hexoquinasa************************************************** -->
        <area alt="hexoquinsa" title="hexoquinasa" href="#openmodal2" coords="560,75,706,109" shape="rect">
    <section id="openmodal2" class="modalDialog">
        <section class="modal2 hola">
            <a href="#close" class="close"> X </a>
            <div class="son2">

                <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play2').play(); return false;"  />
                <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play2').pause(); return false;" />
            </div>
            <?php
            $a = 2;
            $mostrar = new  HomeC();
            $mostrar -> MostrarContenidoC($a);
            ?>

        </section>
    </section>
        <! -- **************************************************Sección Reacción1************************************************** -->
        <area alt="Reacción1" title="Reacción1" href="#openmodal3" coords="562,124,682,155" shape="rect">
        <section id="openmodal3" class="modalDialog">
            <section class="modal3 hola">
                <a href="#close" class="close"> X </a>
                <div class="son3">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play3').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play3').pause(); return false;" />
                </div>
                <?php
                $a = 3;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulacion1***************************************************** -->
        <area alt="regulacion1" title="regulacion1" href="#openmodal4" coords="259,124,386,151" shape="rect">
        <section id="openmodal4" class="modalDialog">
            <section class="modal4 hola">
                <a href="#close" class="close"> X </a>
                <div class="son4">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play4').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play4').pause(); return false;" />
                </div>
                <?php
                $a = 4;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección glucosa6************************************************** -->
        <area alt="glucosa6" title="glucosa6" href="#openmodal5" coords="341,225,594,263" shape="rect">
        <section id="openmodal5" class="modalDialog">
            <section class="modal5 hola">
                <a href="#close" class="close"> X </a>
                <div class="son5">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play5').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play5').pause(); return false;" />
                </div>
                <?php
                $a = 5;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección fosfoglucosa************************************************** -->
        <area alt="fosfoglucosa" title="fosfoglucosa" href="#openmodal6" coords="569,278,812,310" shape="rect">
        <section id="openmodal6" class="modalDialog">
            <section class="modal6 hola">
                <a href="#close" class="close"> X </a>
                <div class="son6">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play6').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play6').pause(); return false;" />
                </div>
                <?php
                $a = 6;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción2************************************************** -->
        <area alt="Reacción 2 " title="Reacción 2 " href="#openmodal7" coords="569,334,692,358" shape="rect">
        <section id="openmodal7" class="modalDialog">
            <section class="modal7 hola">
                <a href="#close" class="close"> X </a>
                <div class="son7">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play7').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play7').pause(); return false;" />
                </div>
                <?php
                $a = 7;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación2************************************************** -->
        <area alt="regulación2" title="regulación2" href="#openmodal8" coords="264,329,387,358" shape="rect">
        <section id="openmodal8" class="modalDialog">
            <section class="modal8 hola">
                <a href="#close" class="close"> X </a>
                <div class="son8">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play8').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play8').pause(); return false;" />
                </div>
                <?php
                $a = 8;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección FRUCTOSA 6-FOSFATO (F6P)************************************************** -->
        <area alt="FRUCTOSA 6-FOSFATO (F6P)" title="FRUCTOSA 6-FOSFATO (F6P)" href="#openmodal9" coords="347,425,593,474" shape="rect">
        <section id="openmodal9" class="modalDialog">
            <section class="modal9 hola">
                <a href="#close" class="close"> X </a>
                <div class="son9">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play9').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play9').pause(); return false;" />
                </div>
                <?php
                $a = 9;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección FOSFOFRUCTOCINASA-1 (EC 2.7.1.11, PFK-1)************************************************** -->
        <area alt="Sección FOSFOFRUCTOCINASA-1 (EC 2.7.1.11, PFK-1)" title="Sección FOSFOFRUCTOCINASA-1 (EC 2.7.1.11, PFK-1) " href="#openmodal10" coords="567,486,776,520" shape="rect">
        <section id="openmodal10" class="modalDialog">
            <section class="modal10 hola">
                <a href="#close" class="close"> X </a>
                <div class="son10">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play10').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play10').pause(); return false;" />
                </div>
                <?php
                $a = 10;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción3************************************************** -->
        <area alt="Reacción 3 " title="Reacción 3 " href="#openmodal11" coords="576,542,695,568" shape="rect">
        <section id="openmodal11" class="modalDialog">
            <section class="modal11 hola">
                <a href="#close" class="close"> X </a>
                <div class="son11">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play11').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play11').pause(); return false;" />
                </div>
                <?php
                $a = 11;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación3************************************************** -->
        <area alt="regulación3" title="regulación3" href="#openmodal12" coords="260,534,391,571" shape="rect">
        <section id="openmodal12" class="modalDialog">
            <section class="modal12 hola">
                <a href="#close" class="close"> X </a>
                <div class="son12">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play12').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play12').pause(); return false;" />
                </div>
                <?php
                $a = 12;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección FRUCTOSA-1,6-DIFOSFATO (FBP)************************************************** -->
        <area alt="FRUCTOSA-1,6-DIFOSFATO (FBP)" title="FRUCTOSA-1,6-DIFOSFATO (FBP)" href="#openmodal13" coords="325,629,610,678" shape="rect">
        <section id="openmodal13" class="modalDialog">
            <section class="modal13 hola">
                <a href="#close" class="close"> X </a>
                <div class="son13">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play13').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play13').pause(); return false;" />
                </div>
                <?php
                $a = 13;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección fosfoglucosa************************************************** -->
        <area alt="ALDOLASA (E.C 4.1.2.13)" title="ALDOLASA (E.C 4.1.2.13)" href="#openmodal14" coords="571,691,661,727" shape="rect">
        <section id="openmodal14" class="modalDialog">
            <section class="modal14 hola">
                <a href="#close" class="close"> X </a>
                <div class="son14">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play14').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play14').pause(); return false;" />
                </div>
                <?php
                $a = 14;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción4************************************************** -->
        <area alt="Reacción 4 " title="Reacción 4 " href="#openmodal15" coords="571,742,692,781" shape="rect">
        <section id="openmodal15" class="modalDialog">
            <section class="modal15 hola">
                <a href="#close" class="close"> X </a>
                <div class="son15">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play15').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play15').pause(); return false;" />
                </div>
                <?php
                $a = 15;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación4************************************************** -->
        <area alt="regulación4" title="regulación4" href="#openmodal16" coords="270,740,386,774" shape="rect">
        <section id="openmodal16" class="modalDialog">
            <section class="modal16 hola">
                <a href="#close" class="close"> X </a>
                <div class="son16">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play16').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play16').pause(); return false;" />
                </div>
                <?php
                $a = 16;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección DIHIDROXIACETONA-FOSFATO (DHAP)************************************************** -->
        <area alt="DIHIDROXIACETONA-FOSFATO (DHAP)" title="DIHIDROXIACETONA-FOSFATO (DHAP)" href="#openmodal17" coords="313,846,631,890" shape="rect">
        <section id="openmodal17" class="modalDialog">
            <section class="modal17 hola">
                <a href="#close" class="close"> X </a>
                <div class="son17">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play17').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play17').pause(); return false;" />
                </div>
                <?php
                $a = 17;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección TRIOSAFOSFATO ISOMERASA (EC 5.3.1.1, TPI)************************************************** -->
        <area alt="TRIOSAFOSFATO ISOMERASA (EC 5.3.1.1, TPI)" title="TRIOSAFOSFATO ISOMERASA (EC 5.3.1.1, TPI)" href="#openmodal18" coords="573,905,801,934" shape="rect">
        <section id="openmodal18" class="modalDialog">
            <section class="modal18 hola">
                <a href="#close" class="close"> X </a>
                <div class="son18">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play18').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play18').pause(); return false;" />
                </div>
                <?php
                $a = 18;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción5************************************************** -->
        <area alt="Reacción 5 " title="Reacción 5 " href="#openmodal19" coords="573,953,695,985" shape="rect">
        <section id="openmodal19" class="modalDialog">
            <section class="modal19 hola">
                <a href="#close" class="close"> X </a>
                <div class="son19">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play19').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play19').pause(); return false;" />
                </div>
                <?php
                $a = 19;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación5************************************************** -->
        <area alt="regulación5" title="regulación5" href="#openmodal20" coords="270,953,389,980" shape="rect">
        <section id="openmodal20" class="modalDialog">
            <section class="modal20 hola">
                <a href="#close" class="close"> X </a>
                <div class="son20">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play20').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play20').pause(); return false;" />
                </div>
                <?php
                $a = 20;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección GLICERALDEHIDO 3-FOSFATO (GAP)************************************************** -->
        <area alt="GLICERALDEHIDO 3-FOSFATO (GAP)" title="GLICERALDEHIDO 3-FOSFATO (GAP)" href="#openmodal21" coords="313,1055,629,1104" shape="rect">
        <section id="openmodal21" class="modalDialog">
            <section class="modal21 hola">
                <a href="#close" class="close"> X </a>
                <div class="son21">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play21').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play21').pause(); return false;" />
                </div>
                <?php
                $a = 21;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección GLICERALDEHIDO 3-FOSFATO DESHIDROGENASA ( EC 1.2.1.12, GAPDH)************************************************** -->
        <area alt="GLICERALDEHIDO 3-FOSFATO DESHIDROGENASA ( EC 1.2.1.12, GAPDH)" title="GLICERALDEHIDO 3-FOSFATO DESHIDROGENASA ( EC 1.2.1.12, GAPDH)" href="#openmodal22" coords="573,1118,811,1148" shape="rect">
        <section id="openmodal22" class="modalDialog">
            <section class="modal22 hola">
                <a href="#close" class="close"> X </a>
                <div class="son22">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play22').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play22').pause(); return false;" />
                </div>
                <?php
                $a = 22;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción6************************************************** -->
        <area alt="Reacción 6 " title="Reacción 6 " href="#openmodal23" coords="578,1172,697,1199" shape="rect">
        <section id="openmodal23" class="modalDialog">
            <section class="modal23 hola">
                <a href="#close" class="close"> X </a>
                <div class="son23">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play23').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play23').pause(); return false;" />
                </div>
                <?php
                $a = 23;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación6************************************************** -->
        <area alt="regulación6" title="regulación6" href="#openmodal24" coords="274,1165,393,1191" shape="rect">
        <section id="openmodal24" class="modalDialog">
            <section class="modal24 hola">
                <a href="#close" class="close"> X </a>
                <div class="son24">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play24').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play24').pause(); return false;" />
                </div>
                <?php
                $a = 24;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección 1,3-BIFOSFOGLICERATO (1,3BPG)************************************************** -->
        <area alt="1,3-BIFOSFOGLICERATO (1,3BPG)" title="1,3-BIFOSFOGLICERATO (1,3BPG)" href="#openmodal25" coords="381,1269,561,1305" shape="rect">
        <section id="openmodal25" class="modalDialog">
            <section class="modal25 hola">
                <a href="#close" class="close"> X </a>
                <div class="son25">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play25').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play25').pause(); return false;" />
                </div>
                <?php
                $a = 25;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección FOSFOGLICERATO QUINASA (EC 2.7.2.3, PGK)************************************************** -->
        <area alt="FOSFOGLICERATO QUINASA (EC 2.7.2.3, PGK)" title="FOSFOGLICERATO QUINASA (EC 2.7.2.3, PGK) " href="#openmodal26" coords="580,1322,801,1352" shape="rect">
        <section id="openmodal26" class="modalDialog">
            <section class="modal26 hola">
                <a href="#close" class="close"> X </a>
                <div class="son26">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play26').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play26').pause(); return false;" />
                </div>
                <?php
                $a = 26;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción7************************************************** -->
        <area alt="Reacción 7 " title="Reacción 7 " href="#openmodal27" coords="578,1373,697,1405" shape="rect">
        <section id="openmodal27" class="modalDialog">
            <section class="modal27 hola">
                <a href="#close" class="close"> X </a>
                <div class="son27">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play27').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play27').pause(); return false;" />
                </div>
                <?php
                $a = 27;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación7************************************************** -->
        <area alt="regulación7" title="regulación7" href="#openmodal28" coords="277,1369,396,1398" shape="rect">
        <section id="openmodal28" class="modalDialog">
            <section class="modal28 hola">
                <a href="#close" class="close"> X </a>
                <div class="son28">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play28').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play28').pause(); return false;" />
                </div>
                <?php
                $a = 28;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección 3-FOSFOGLICERATO (PGA)************************************************** -->
        <area alt="3-FOSFOGLICERATO (PGA)" title="3-FOSFOGLICERATO (PGA)" href="#openmodal29" coords="386,1468,585,1514" shape="rect">
        <section id="openmodal29" class="modalDialog">
            <section class="modal29 hola">
                <a href="#close" class="close"> X </a>
                <div class="son29">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play29').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play29').pause(); return false;" />
                </div>
                <?php
                $a = 29;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección FOSFOGLICERATO MUTASA (EC 5.4.2.1, PGAM)************************************************** -->
        <area alt="FOSFOGLICERATO MUTASA (EC 5.4.2.1, PGAM)" title="FOSFOGLICERATO MUTASA (EC 5.4.2.1, PGAM)" href="#openmodal30" coords="580,1529,816,1568" shape="rect">
        <section id="openmodal30" class="modalDialog">
            <section class="modal30 hola">
                <a href="#close" class="close"> X </a>
                <div class="son30">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play30').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play30').pause(); return false;" />
                </div>
                <?php
                $a = 30;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción8************************************************** -->
        <area alt="Reacción 8 " title="Reacción 8 " href="#openmodal31" coords="588,1585,709,1614" shape="rect">
        <section id="openmodal31" class="modalDialog">
            <section class="modal31 hola">
                <a href="#close" class="close"> X </a>
                <div class="son31">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play31').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play31').pause(); return false;" />
                </div>
                <?php
                $a = 31;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación8************************************************** -->
        <area alt="regulación8" title="regulación8" href="#openmodal32" coords="274,1580,406,1609" shape="rect">
        <section id="openmodal32" class="modalDialog">
            <section class="modal32 hola">
                <a href="#close" class="close"> X </a>
                <div class="son32">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play32').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play32').pause(); return false;" />
                </div>
                <?php
                $a = 32;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección 2-FOSFOGLICERATO (2PG)************************************************** -->
        <area alt="2-FOSFOGLICERATO (2PG)" title="2-FOSFOGLICERATO (2PG)" href="#openmodal33" coords="384,1685,590,1727" shape="rect">
        <section id="openmodal33" class="modalDialog">
            <section class="modal33 hola">
                <a href="#close" class="close"> X </a>
                <div class="son33">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play33').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play33').pause(); return false;" />
                </div>
                <?php
                $a = 33;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección ENOLASA (EC 4.2.1.11)************************************************** -->
        <area alt="ENOLASA (EC 4.2.1.11)" title="ENOLASA (EC 4.2.1.11)" href="#openmodal34" coords="588,1741,682,1773" shape="rect">
        <section id="openmodal34" class="modalDialog">
            <section class="modal34 hola">
                <a href="#close" class="close"> X </a>
                <div class="son34">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play34').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play34').pause(); return false;" />
                </div>
                <?php
                $a = 34;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción9************************************************** -->
        <area alt="Reacción 9 " title="Reacción 9 " href="#openmodal35" coords="590,1792,714,1826" shape="rect">
        <section id="openmodal35" class="modalDialog">
            <section class="modal35 hola">
                <a href="#close" class="close"> X </a>
                <div class="son35">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play35').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play35').pause(); return false;" />
                </div>
                <?php
                $a = 35;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación9************************************************** -->
        <area alt="regulación9" title="regulación9" href="#openmodal36" coords="284,1787,413,1821" shape="rect">
        <section id="openmodal36" class="modalDialog">
            <section class="modal36 hola">
                <a href="#close" class="close"> X </a>
                <div class="son36">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play36').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play36').pause(); return false;" />
                </div>
                <?php
                $a = 36;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección FOSFOENOLPIRUVATO (PEP)************************************************** -->
        <area alt="FOSFOENOLPIRUVATO (PEP)" title="FOSFOENOLPIRUVATO (PEP)" href="#openmodal37" coords="367,1887,597,1931" shape="rect">
        <section id="openmodal37" class="modalDialog">
            <section class="modal37 hola">
                <a href="#close" class="close"> X </a>
                <div class="son37">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play37').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play37').pause(); return false;" />
                </div>
                <?php
                $a = 37;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección PIRUVATO QUINASA (EC 2.7.1.40, PKLR, PKM1/M2))************************************************** -->
        <area alt="PIRUVATO QUINASA (EC 2.7.1.40, PKLR, PKM1/M2)" title="PIRUVATO QUINASA (EC 2.7.1.40, PKLR, PKM1/M2)" href="#openmodal38" coords="597,1950,760,1984" shape="rect">
        <section id="openmodal38" class="modalDialog">
            <section class="modal38 hola">
                <a href="#close" class="close"> X </a>
                <div class="son38">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play38').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play38').pause(); return false;" />
                </div>
                <?php
                $a = 38;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección Reacción10************************************************** -->
        <area alt="Reacción 10 " title="Reacción 10" href="#openmodal39" coords="595,2001,712,2028" shape="rect">
        <section id="openmodal39" class="modalDialog">
            <section class="modal39 hola">
                <a href="#close" class="close"> X </a>
                <div class="son39">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play39').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play39').pause(); return false;" />
                </div>
                <?php
                $a = 39;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección regulación9************************************************** -->
        <area alt="regulación10" title="regulación10" href="#openmodal40" coords="284,2001,406,2028" shape="rect">
        <section id="openmodal40" class="modalDialog">
            <section class="modal40 hola">
                <a href="#close" class="close"> X </a>
                <div class="son40">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play40').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play40').pause(); return false;" />
                </div>
                <?php
                $a = 40;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>
        <! -- **************************************************Sección PIRUVATO************************************************** -->
        <area alt="PIRUVATO" title="PIRUVATO" href="#openmodal41" coords="420,2083,556,2124" shape="rect">
        <section id="openmodal41" class="modalDialog">
            <section class="modal41 hola">
                <a href="#close" class="close"> X </a>
                <div class="son41">

                    <img src="Vistas/img/play.png" onClick="document.getElementById('audio_play41').play(); return false;"  />
                    <img src="Vistas/img/pause.png" onClick="document.getElementById('audio_play41').pause(); return false;" />
                </div>
                <?php
                $a = 41;
                $mostrar = new  HomeC();
                $mostrar -> MostrarContenidoC($a);
                ?>

            </section>
        </section>


    </map>
</div>
</div>

