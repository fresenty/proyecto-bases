<?php


session_start();

if(!$_SESSION["ADMIN"]){

    header("location:index.php?ruta=ingreso");

    exit();
}

?>
<?php
$dat = new ResultadosC();
$dat -> LlenarScoreC();
?>

<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'No se como es que es'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Chrome',
                y: 61.41,
                sliced: true,
                selected: true
            }, {
                name: 'Internet Explorer',
                y: 11.84
            }, {
                name: 'Firefox',
                y: 10.85
            }, {
                name: 'Edge',
                y: 4.67
            }, {
                name: 'Safari',
                y: 4.18
            }, {
                name: 'Sogou Explorer',
                y: 1.64
            }, {
                name: 'Opera',
                y: 1.6
            }, {
                name: 'QQ',
                y: 1.2
            }, {
                name: 'Other',
                y: 2.61
            }]
        }]
    });
</script>

<br>
<h1>Datos del estudiante</h1>
<table >
    <thead>
    <tr>
        <th>Identificacion</th>
        <th>Nombres</th>
        <th>Apellidos</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $dato = new ResultadosC();
    $dato -> MostrarDatosC();
    ?>
    </tbody>
</table>
<br>
<h1>Resultados</h1>

<table >
    <thead>
    <tr>
        <th>Pregunta</th>
        <th>Respuesta correcta</th>
        <th>Respuesta marcada</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $mostrar = new ResultadosC();
    $mostrar -> MostrarRespuestaC();
    ?>
    </tbody>
</table>
<table style="font-size: 30px;">
    <thead>
    <tr>
        <th>NOTA</th>
        <th><?php
            $nota = new ResultadosC();
            $nota -> MostrarNotaC();
            ?> correctas</th>
    </tr>
    </thead>
</table>

