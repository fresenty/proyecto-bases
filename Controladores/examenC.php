<?php


class ExamenC{
    //quiz
    public function BorrarDatosC(){
        $datosC = $_SESSION["cedula"];
        ExamenM::BorrarDatosM($datosC);
    }

    public function PreguntasC(){
        //Pregunta aleatoria
        $ray = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
        $cont = 18;
        $d = 17;
        $datosCI = $_SESSION["cedula"];
        while ($cont > 10) {
            $rn = rand(0,$d);
            unset($ray[$rn]);
            sort($ray);
            $cont = $cont - 1;
            $d = $d - 1;
        }
        //Acceso a las preguntas
        if(isset($_POST["r1"]) and isset($_POST["pre1"])){
            $datosG1 = array("cedula" => $datosCI, "id_pre" => $_POST["pre1"], "res_e"=>$_POST["r1"]);
            $datosG2 = array("cedula" => $datosCI, "id_pre" => $_POST["pre2"], "res_e"=>$_POST["r2"]);
            $datosG3 = array("cedula" => $datosCI, "id_pre" => $_POST["pre3"], "res_e"=>$_POST["r3"]);
            $datosG4 = array("cedula" => $datosCI, "id_pre" => $_POST["pre4"], "res_e"=>$_POST["r4"]);
            $datosG5 = array("cedula" => $datosCI, "id_pre" => $_POST["pre5"], "res_e"=>$_POST["r5"]);
            $datosG6 = array("cedula" => $datosCI, "id_pre" => $_POST["pre6"], "res_e"=>$_POST["r6"]);
            $datosG7 = array("cedula" => $datosCI, "id_pre" => $_POST["pre7"], "res_e"=>$_POST["r7"]);
            $datosG8 = array("cedula" => $datosCI, "id_pre" => $_POST["pre8"], "res_e"=>$_POST["r8"]);
            $datosG9 = array("cedula" => $datosCI, "id_pre" => $_POST["pre9"], "res_e"=>$_POST["r9"]);
            $datosG10 = array("cedula" => $datosCI, "id_pre" => $_POST["pre10"], "res_e"=>$_POST["r10"]);
            $tablaGBD = "presenta";
            ExamenM::GuardarRespuestasM($datosG1, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG2, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG3, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG4, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG5, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG6, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG7, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG8, $tablaGBD);
            ExamenM::GuardarRespuestasM($datosG9, $tablaGBD);
            $respuesta = ExamenM::GuardarRespuestasM($datosG10, $tablaGBD);
            if($respuesta == "Bien"){
                header("location:index.php?ruta=resultados");
            }else{
                header("location:index.php?ruta=resultados");
            }
        }else{
            echo '<h2><u>AREA DE PREGUNTAS</u></h2>';
            for ($i=0; $i < 10; $i++) {
                $nu = $i + 1;
                $datosC = intval($ray[$i]);
                $tablaBD = "pregunta";
                $respuesta = ExamenM::PreguntasM($datosC, $tablaBD);
                echo ''.$nu.''.$respuesta["des_pre"].'<br>
				    '.$respuesta["res_a"].'<br>
				    '.$respuesta["res_b"].'<br>
				    '.$respuesta["res_c"].'<br>
				    '.$respuesta["res_d"].'<br><br>';
            }
            echo ' <h2><u>AREA PARA RESPUESTAS</u></h2>';
            echo ' <h4> a = 1,	b = 2,	c = 3,	d = 4 </h4>';
            echo '<form method="post">

	1      <input type="number" min="1" max="4" name="r1" required>
	2      <input type="number" min="1" max="4" name="r2" required>
	3      <input type="number" min="1" max="4" name="r3" required>
	4      <input type="number" min="1" max="4" name="r4" required><br>
	5      <input type="number" min="1" max="4" name="r5" required> 
	6      <input type="number" min="1" max="4" name="r6" required>
	7      <input type="number" min="1" max="4" name="r7" required>
	8      <input type="number" min="1" max="4" name="r8" required><br>
	9      <input type="number" min="1" max="4" name="r9" required>
	10     <input type="number" min="1" max="4" name="r10" required><br><br>
	<input type="hidden" value="'.$ray[0].'" name="pre1">
	<input type="hidden" value="'.$ray[1].'" name="pre2">
	<input type="hidden" value="'.$ray[2].'" name="pre3">
	<input type="hidden" value="'.$ray[3].'" name="pre4">
	<input type="hidden" value="'.$ray[4].'" name="pre5">
	<input type="hidden" value="'.$ray[5].'" name="pre6">
	<input type="hidden" value="'.$ray[6].'" name="pre7">
	<input type="hidden" value="'.$ray[7].'" name="pre8">
	<input type="hidden" value="'.$ray[8].'" name="pre9">
	<input type="hidden" value="'.$ray[9].'" name="pre10">
	<input type="submit" value="Responder"></form>';
        }
    }
}
?>