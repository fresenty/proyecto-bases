<?php 

class AdminC
{

    public function IngresoC()
    {
        // INGRESO ADMINISTRADOR

        if (isset($_POST["tipoI"])) {


            if ($_POST["tipoI"] === "admin" ||$_POST["tipoI"] === "cedula" || $_POST["tipoI"] === "administrador" || $_POST["tipoI"] === "ADMIN" || $_POST["tipoI"] === "ADMINISTRADOR") {

                if (isset($_POST["usuarioI"])) {

                    $datosC = array("tipo" => $_POST["tipoI"], "usuario" => $_POST["usuarioI"], "cedula" => $_POST["cedulaI"],  "clave" => $_POST["claveI"]);

                    $tablaBD = "persona";

                    $tipop = "1";

                    $respuesta = AdminM::IngresoM($datosC, $tablaBD, $tipop);


                    if ($respuesta["usuario"] == $_POST["usuarioI"] && $respuesta["clave"] == $_POST["claveI"]) {

                        session_start();
                        $_SESSION["cedula"] = $_POST["cedulaI"];

                        $_SESSION["ADMIN"] = true;

                        header("location:index.php?ruta=home");

                    } else {

                        echo "ERROR AL INGRESAR";
                    }
                }
            }

            // INGRESO ESTUDIANTE

            if ($_POST["tipoI"] === "estudiante" || $_POST["tipoI"] === "ESTUDIANTE" || $_POST["tipoI"] === "Estudiante" || $_POST["tipoI"] === "ESTUDIANTE") {

                if (isset($_POST["usuarioI"])) {

                    $datosC = array("tipo" => $_POST["tipoI"], "usuario" => $_POST["usuarioI"], "clave" => $_POST["claveI"]);

                    $tablaBD = "persona";

                    $tipop = "2";

                    $respuesta = AdminM::IngresoM($datosC, $tablaBD, $tipop);

                    if ($respuesta["usuario"] == $_POST["usuarioI"] && $respuesta["clave"] == $_POST["claveI"]) {

                        session_start();

                        $_SESSION["ESTUDIANTE"] = true;

                        header("location:index.php?ruta=home");

                    } else {

                        echo "ERROR AL INGRESAR";
                    }
                }
            }


        }
    }
}