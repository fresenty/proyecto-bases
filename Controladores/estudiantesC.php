<?php
class EstudiantesC
{

    //Registrar los Estudiantes

    public function RegistrarEstudiantesC()
    {

        if (isset($_POST["cedulaR"])) {  //si la variable viene con registro

            $datosC = array("cedula" => $_POST["cedulaR"], "usuario" => $_POST["usuarioR"], "nombres" => $_POST["nombresR"], "apellidos" => $_POST["apellidosR"], "edad" => $_POST["edadR"], "sexo" => $_POST["sexoR"]);

            $tablaBD = "persona";

            $respuesta = EstudiantesM::RegistrarEstudiantesM($datosC, $tablaBD);

            if ($respuesta == "Bien") {

                header("location:index.php?ruta=estudiantes");

            } else {

                echo "error";
            }

        }

    }

    //Registrar las Credenciales

    public function RegistrarCredencialesC()
    {

        if (isset($_POST["cedulaR"])) {  //si la variable viene con registro

            $datosC = array("cedula" => $_POST["cedulaR"], "correo" => $_POST["correoR"], "clave" => $_POST["claveR"]);

            $tablaBD = "credenciales";

            $respuesta = EstudiantesM::RegistrarCredencialesM($datosC, $tablaBD);

            if ($respuesta == "Bien") {

                header("location:index.php?ruta=estudiantes");

            } else {

                echo "error";
            }

        }

    }

    //Mostrar Estudiantes
    public function MostrarEstudiantesC()
    {

        $tablaBD = "persona";

        $respuesta = EstudiantesM::MostrarEstudiantesM($tablaBD);

        foreach ($respuesta as $key => $value) {

            echo '<tr>
					<td>' . $value["cedula"] . '</td>
					<td>' . $value["usuario"] . '</td>
					<td>' . $value["correo"] . '</td>
					<td>' . $value["clave"] . '</td>
					<td>' . $value["nombres"] . '</td>
					<td>' . $value["apellidos"] . '</td>
					<td>' . $value["edad"] . '</td>
					<td>' . $value["sexo"] . '</td>
					<td><a class="myButton" href="index.php?ruta=editar&cedula=' . $value["cedula"] . '"><button>Editar</button></a></td>
					<td><a class="myButton" href="index.php?ruta=estudiantes&cedulaB=' . $value["cedula"] . '"><button>Borrar</button></a></td>
			      </tr>';

        }

    }

    //Editar Estudiante

    public function EditarEstudiantesC()
    {

        $datosC = $_GET["cedula"];
        $tablaBD = "persona";

        $respuesta = EstudiantesM::EditarEstudiantesM($datosC, $tablaBD);

        echo '<input type="hidden" value="' . $respuesta["cedula"] . '" name="cedulaE">

		  <input type="text" placeholder="Cedula" value="' . $respuesta["cedula"] . '" name="cedulaE" required>

   		  <input type="text" placeholder="Usuario" value="' . $respuesta["usuario"] . '" name="usuarioE" required>
   		  
   		  <input type="text" placeholder="Correo" value="' . $respuesta["correo"] . '" name="correoE" required>
   		  
   		  <input type="text" placeholder="Clave" value="' . $respuesta["clave"] . '" name="claveE" required>
 
		  <input type="text" placeholder="Nombres" value="' . $respuesta["nombres"] . '" name="nombresE" required>

		  <input type="text" placeholder="Apellidos" value="' . $respuesta["apellidos"] . '" name="apellidosE" required>

		  <input type="text" placeholder="Edad" value="' . $respuesta["edad"] . '" name="edadE" required>
		  
		  <input type="text" placeholder="sexo" value="' . $respuesta["sexo"] . '" name="sexoE" required>

		  <input type="submit" value="Actualizar">';
    }

    //Actualizar Estudiantes

    public function ActualizarEstudiantesC()
    {

        if (isset($_POST["cedulaE"])) {

            $datosC = array("cedula" => $_POST["cedulaE"], "usuario" => $_POST["usuarioE"], "correo" => $_POST["correoE"], "clave" => $_POST["claveE"],  "nombres" => $_POST["nombresE"], "apellidos" => $_POST["apellidosE"], "edad" => $_POST["edadE"], "sexo" => $_POST["sexoE"]);


            $tablaBD = "persona";
            $respuesta = EstudiantesM::ActualizarEstudiantesM($datosC, $tablaBD);


            if ($respuesta == "Bien") {

                header("location:index.php?ruta=estudiantes");

            } else {

                echo "error";


            }
        }
    }

    //Eliminar Estudiantes

    public function BorrarEstudiantesC(){


        if (isset($_GET["cedulaB"])) {

            $datosC = $_GET["cedulaB"];

            $tablaBD = "persona";

            $respuesta = EstudiantesM::BorrarEstudiantesM($datosC, $tablaBD);

            if ($respuesta == "Bien") {

                header("location:index.php?ruta=estudiantes");

            } else {

                echo "error";


            }

        }

    }
    //vista mascu

    public function vistaMascuC(){

        $tablaBD = "vista_mascu";

        $respuesta = estudiantesM::vistamascuM($tablaBD );

        foreach ($respuesta as $key => $value) {

            echo ("<h2> Cantidad de personas del sexo Masculino:". $value["COUNT(*)"] . "</h2>");

        }
    }

}