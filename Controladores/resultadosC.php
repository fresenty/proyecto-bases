<?php
class ResultadosC{
    public function LlenarScoreC(){
        $datosC = $_SESSION["cedula"];
        $rest = substr(implode(ResultadosM::RespuetasVM($datosC)),1);
        $res = intval($rest);
        $respuesta = ResultadosM::LlenarScoreM($datosC,$res);
    }

    public function MostrarDatosC(){
        $datosC = $_SESSION["cedula"];
        $tablaBD = "persona";
        $respuesta = ResultadosM::MostrarDatosM($datosC, $tablaBD);
        echo '<tr>
					<td>'.$respuesta["cedula"].'</td>
					<td>'.$respuesta["nombres"].'</td>
					<td>'.$respuesta["apellidos"].'</td>
				</tr>';
    }

    public function MostrarRespuestaC(){
        $datosC = $_SESSION["cedula"];
        $respuesta = ResultadosM::MostrarRespuestasM($datosC);
        foreach ($respuesta as $key => $value) {
            echo '<tr>
					<td>'.$value["des_pre"].'</td>
					<td>'.$value["res_v"].'</td>
					<td>'.$value["res_e"].'</td>
				</tr>';
        }
    }
    public function MostrarNotaC(){
        $datosC = $_SESSION["cedula"];
        $respuesta = ResultadosM::MostrarNotaM($datosC);
        echo ''.$respuesta["nota"].'';
    }


}

?>